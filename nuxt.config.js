module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'scme',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'SCME new site' },
      { hid: 'msapplication-TileColor', name: 'msapplication-TileColor', content: '#51893e' },
      {
        hid: 'msapplication-TileImage',
        name: 'msapplication-TileImage',
        content: '/ms-icon-144x144.png'
      },
      { hid: 'theme-color', name: 'theme-color', content: '#51893e' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '57x57', href: '/apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: '/apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '72x72', href: '/apple-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/apple-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '114x114', href: '/apple-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '144x144', href: '/apple-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-icon-180x180.png' },
      { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/android-icon-192x192.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/android-icon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '96x96', href: '/android-icon-96x96.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/android-icon-16x16.png' },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'preload', as: 'font', href: 'https://fonts.googleapis.com/css?family=Lato' }
    ],
    script: [
      {
        src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCXtGNc3DJFhQ6QgH3XiRjn6AoRRBjrkig'
      }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#51893e' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    },
    vendor: ['axios'] // Add axios in the vendor.bundle.js
  },
  css: [
    // SCSS file in the project
    '@/assets/scss/main.scss'
  ],
  env: {
    SPACE_ID: 'v5d03q51whq7',
    ACCESS_TOKEN: 'e30a131b4e228fe71c803fe2f6b588577df25e85c0f4349ce436af43cdaff440'
  },
  plugins: ['~/plugins/vue-tabs-component.js']
};
