// eslint-disable-next-line  import/no-extraneous-dependencies
import Vue from 'vue';
import { Tabs, Tab } from 'vue-tabs-component';

Vue.component('tabs', Tabs);
Vue.component('tab', Tab);
