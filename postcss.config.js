module.exports = {
  plugins: {
    'postcss-cssnext': {
      features: {
        customProperties: false
      }
    }
  }
};
